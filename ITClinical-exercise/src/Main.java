import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String word = "ITCLiNicAl";
        String specialWord = "!tCL1Nical";
        String wordLog = "ItCLINiCAL";

        System.out.println("---MAIN EXERCISE---");
        System.out.println(getWord(word, 1));
        System.out.println(getWord(word, 2));
        System.out.println(getWord(word, 3));
        System.out.println(getWord(word, 100));
        System.out.println(getWord(word, -1));

        System.out.println("---OPTIONAL TASK 1---");
        System.out.println(getWordWithSymbolsAndNumbers(specialWord, 1));

        System.out.println("---OPTIONAL TASK 2---");
        getWordLog(wordLog, 1);
    }

    private static String getWord(String word, int N){
        StringBuilder result = new StringBuilder();
        String characterCheck;
        int counter = 1;

        if(N > word.length() || N < 0){
            return result.toString();
        }

        for (int i = 0; i < word.length(); i++) {

            if(counter == N){
                if(word.charAt(i) != 'N' && Character.isUpperCase(word.charAt(i))){
                    result.append(word.charAt(i));
                }
                counter = 0;
            }

            characterCheck = "" +  word.charAt(i);

            if(characterCheck.equals("N")){
                result.append(word.charAt(i));
            }

            counter++;
        }

        return result.toString();
    }

    //Optional Task 1
    private static String getWordWithSymbolsAndNumbers(String word, int N){
        String specialCharacters=" !#$%&'()*+,-./:;<=>?@[]^_`{|}";
        StringBuilder result = new StringBuilder();
        String characterCheck;
        int counter = 1;

        if(N > word.length() || N < 0){
            return result.toString();
        }

        for (int i = 0; i < word.length(); i++) {

            if(counter == N){
                if(word.charAt(i) != 'N' && Character.isUpperCase(word.charAt(i)) || specialCharacters.contains(Character.toString(word.charAt(i))) || Character.isDigit(word.charAt(i))){
                    result.append(word.charAt(i));
                }
                counter = 0;
            }

            characterCheck = "" +  word.charAt(i);

            if(characterCheck.equals("N")){
                result.append(word.charAt(i));
            }

            counter++;
        }

        return result.toString();
    }

    ////Optional Task 2
    private static void getWordLog(String word, int N){
        Map<Character, Integer> characters = new LinkedHashMap<>();
        StringBuilder result = new StringBuilder();
        String characterCheck;
        int counter = 1;

        if(N > word.length() || N < 0){
            System.out.println(result);
        }

        for (int i = 0; i < word.length(); i++) {

            if(counter == N){
                if(word.charAt(i) != 'N' && Character.isUpperCase(word.charAt(i))){
                    result.append(word.charAt(i));
                }
                counter = 0;
            }

            characterCheck = "" +  word.charAt(i);

            if(characterCheck.equals("N")){
                result.append(word.charAt(i));
            }

            counter++;
        }

        for (int i = 0; i < result.length(); i++) {
            if(characters.containsKey(result.charAt(i))){
                characters.put(result.charAt(i), characters.get(result.charAt(i)) + 1);
            }else {
                characters.put(result.charAt(i), 1);
            }
        }

        characters.forEach((key, value) -> System.out.println(key + " = " + value));
    }

}